/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pasin.inheritance;

/**
 *
 * @author Pla
 */
public class TestAnimal {

    public static void main(String[] args) {
        Animal animal = new Animal("Ani", "White", 0);
        animal.speak();
        animal.walk();

        Dog dang = new Dog("Dang", "BlackWhite");
        dang.speak();
        dang.walk();
        
        Dog to =new Dog("To","Brown");
        to.speak();
        to.walk();
        
        Dog mome =new Dog("Mome","BlackWhite");
        mome.speak();
        mome.walk();
        
        Dog bat =new Dog("Bat","BlavkWhite");
        bat.speak();
        bat.walk();

       
        
        Cat som = new Cat("Som", "Orange");
        som.speak();
        som.walk();

        Duck ped = new Duck("Ped", "Orange");
        ped.speak();
        ped.walk();
        ped.fly();
        
        Duck cj = new Duck("Cj", "BlackYellow");
        ped.speak();
        ped.walk();
        ped.fly();
        
        
        System.out.println("----Cheack----");
        System.out.println("Ped is Animal : "+(ped instanceof Animal));
        System.out.println("Ped is Duck : "+(ped instanceof Duck));
        System.out.println("Ped is Cat : "+(ped instanceof Object));
        System.out.println("Animal is Dog : "+(animal instanceof Dog));
        System.out.println("Animal is Animal : "+(animal instanceof Animal));
        
        System.out.println("---- For Loop ----");
        
        Animal[] animals = {dang,to,mome,bat,som,ped};
        for(int i=0;i<animals.length;i++){
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck){
                Duck duck =(Duck)animals[i];
                duck.fly();
            }
        }
        
    }
}
